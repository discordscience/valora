/*

    Valora AI Moderator Bot
    Copyright (C) 2018  Discord Science and Technology Community

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package discordscience.perspectiveapi;

import com.google.common.util.concurrent.RateLimiter;
import org.json.*;
import java.net.*;
import java.util.*;
import java.util.stream.*;
import jdk.incubator.http.*;
import lombok.*;

@lombok.extern.log4j.Log4j2
public class PerspectiveAPI {

    private final String URL_ANALYZE = "https://commentanalyzer.googleapis.com/v1alpha1/comments:analyze?key=";
    private final String URL_SUGGEST_SCORE = "https://commentanalyzer.googleapis.com/v1alpha1/comments:suggestscore?key=";

    private final RateLimiter rateLimiter;
    private final HttpClient client;
    private final String key, communityId;

    public PerspectiveAPI(String key, String communityId) {
        this.key = key;
        this.communityId = communityId;
        rateLimiter = RateLimiter.create(9.0);
        client = HttpClient.newHttpClient();
    }

    private void rateLimit() {
        if(!rateLimiter.tryAcquire()) {
            log.warn("Rate-limit reached!");
            rateLimiter.acquire();
        }
    }

    @SneakyThrows
    public void suggestCommentScore(String comment, Map<Attribute, Double> values) {
        rateLimit();

        // Make HTTP request
        JSONObject requestObj = new JSONObject();
        requestObj.put("comment", new RequestComment(comment).toJSON());
        requestObj.put("languages", new JSONArray(Arrays.asList("en")));
        requestObj.put("communityId", communityId);
        Map<Attribute, AttributeScore> scores = new HashMap<>();
        values.entrySet().forEach(e -> scores.put(e.getKey(), new AttributeScore(e.getValue().doubleValue())));
        requestObj.put("attributeScores", new AttributeScoreList(new HashMap<>(), scores).toJSON());
        HttpRequest request = HttpRequest.newBuilder()
                              .uri(new URI(URL_SUGGEST_SCORE + key))
                              .headers("Content-Type", "application/json")
                              .POST(HttpRequest.BodyPublisher.fromString(requestObj.toString()))
                              .build();
        log.debug(requestObj.toString()); // DEBUG

        // Check HTTP response is status-code 200
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandler.asString());
        if(response.statusCode() != 200) {
            throw new RuntimeException("PerspectiveAPI returned non-200 status code." + System.lineSeparator() + response.body());
        }

        // DEBUG: log.debug(response.body());

        // Do not care about response. It is empty-ish anyways.
    }

    @SneakyThrows
    public AttributeScoreList analyzeComment(String comment, Attribute... requested) {
        rateLimit();

        // Make HTTP request
        JSONObject requestObj = new JSONObject();
        requestObj.put("comment", new RequestComment(comment).toJSON());
        requestObj.put("languages", new JSONArray(Arrays.asList("en")));
        requestObj.put("requestedAttributes", new RequestAttributeList(requested).toJSON());
        HttpRequest request = HttpRequest.newBuilder()
                              .uri(new URI(URL_ANALYZE + key))
                              .headers("Content-Type", "application/json")
                              .POST(HttpRequest.BodyPublisher.fromString(requestObj.toString()))
                              .build();

        // Check HTTP response is status-code 200
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandler.asString());
        if(response.statusCode() != 200) {
            throw new RuntimeException("PerspectiveAPI returned non-200 status code." + System.lineSeparator() + response.body());
        }

        // DEBUG: log.debug(response.body());

        return json2asl(response.body());
    }

    public AttributeScoreList json2asl(String str) {
        // parse JSON
        JSONObject obj = (JSONObject) new JSONTokener(str).nextValue();
        obj = obj.getJSONObject("attributeScores");

        // create data maps
        Map<Attribute, SpanScoreList> spanScores = new HashMap<>();
        Map<Attribute, AttributeScore> summaryScores = new HashMap<>();

        // go through each attribute
        for(Attribute at : Attribute.values()) {
            // check if present
            if(!obj.has(at.name())) {
                continue;
            }

            // add new empty span list
            spanScores.put(at, new SpanScoreList());

            // Attribute result part
            JSONObject atp = obj.getJSONObject(at.name());
            JSONArray spanScoreArray = atp.getJSONArray("spanScores");
            for(int i = 0; i != spanScoreArray.length(); i++) {
                spanScores.get(at).add(parseSpanScore(spanScoreArray.getJSONObject(i)));
            }
            summaryScores.put(at, parseAttributeScore(atp.getJSONObject("summaryScore")));
        }
        return new AttributeScoreList(spanScores, summaryScores);
    }

    private SpanScore parseSpanScore(JSONObject obj) {
        return new SpanScore(obj.getInt("begin"), obj.getInt("end"), parseAttributeScore(obj.getJSONObject("score")));
    }

    private AttributeScore parseAttributeScore(JSONObject obj) {
        return new AttributeScore(obj.getDouble("value"), obj.getString("type"));
    }

    @Data
    public class RequestAttributeList {
        private final Attribute[] list;

        public JSONObject toJSON() {
            JSONObject obj = new JSONObject();
            for(Attribute a : list) {
                obj.put(a.name(), new JSONObject());
            }
            return obj;
        }
    }

    @Data @RequiredArgsConstructor @AllArgsConstructor
    public class RequestComment {
        private final String text;
        private String type = "PLAIN_TEXT";

        public JSONObject toJSON() {
            JSONObject obj = new JSONObject();
            obj.put("text", text);
            if(!type.equals("PLAIN_TEXT")) {
                obj.put("type", type);
            }
            return obj;
        }
    }

    @RequiredArgsConstructor
    public class AttributeScoreList {

        @Getter private final Map<Attribute, SpanScoreList> spanScores;
        @Getter private final Map<Attribute, AttributeScore> summaryScores;

        public SpanScoreList getSpanScores(Attribute attribute) {
            return spanScores.get(attribute);
        }

        public AttributeScore getSummaryScore(Attribute attribute) {
            return summaryScores.get(attribute);
        }

        public JSONObject toJSON() {
            JSONObject obj = new JSONObject();
            for(Attribute a : Attribute.values()) {
                JSONObject scores = new JSONObject();
                if(summaryScores.containsKey(a)) {
                    scores.put("summaryScore", summaryScores.get(a).toJSON());
                }
                if(spanScores.containsKey(a)) {
                    scores.put("spanScores", spanScores.get(a).toJSON());
                }
                if(scores.length() > 0) {
                    obj.put(a.name(), scores);
                }
            }
            return obj;
        }

    }

    public class SpanScoreList extends ArrayList<SpanScore> {

        public JSONArray toJSON() {
            JSONArray array = new JSONArray();
            for(Object o : this) {
                array.put(((SpanScore) o).toJSON());
            }
            return array;
        }
    }

    @Data
    public class SpanScore {
        private final int begin, end;
        private final AttributeScore score;

        public JSONObject toJSON() {
            JSONObject obj = new JSONObject();
            obj.put("begin", begin);
            obj.put("end", end);
            obj.put("score", score.toJSON());
            return obj;
        }
    }

    @Data @RequiredArgsConstructor @AllArgsConstructor
    public class AttributeScore {
        private final double value;
        private String type = "PROBABILITY";

        public JSONObject toJSON() {
            JSONObject obj = new JSONObject();
            obj.put("value", value);
            obj.put("type", type);
            return obj;
        }
    }

}
