/*

    Valora AI Moderator Bot
    Copyright (C) 2018  Discord Science and Technology Community

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package discordscience.valora;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.*;
import net.cubekrowd.eventstorageapi.api.*;
import net.dv8tion.jda.core.*;
import net.dv8tion.jda.core.hooks.*;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.*;
import net.dv8tion.jda.core.events.message.*;
import net.dv8tion.jda.core.events.message.guild.*;
import net.dv8tion.jda.core.events.message.guild.react.*;
import net.dv8tion.jda.core.events.message.priv.*;
import net.dv8tion.jda.core.events.message.priv.react.*;
import net.dv8tion.jda.core.events.guild.member.*;
import net.dv8tion.jda.core.events.guild.*;
import discordscience.perspectiveapi.*;
import lombok.*;

@lombok.extern.log4j.Log4j2
@RequiredArgsConstructor
public class ImageListener extends ListenerAdapter {

    private final ValoraBot bot;

    @Override
    public void onReady(ReadyEvent event) {
	File stordir = new File("data_images");
	stordir.mkdir();
    }

    @Override
    public void onGuildMessageReceived(@NonNull GuildMessageReceivedEvent event) {
        gotMessage(event.getAuthor(), event.getMessage(), event.getGuild(), event.getChannel());
    }
    //@Override
    //public void onPrivateMessageReceived(@NonNull PrivateMessageReceivedEvent event) {
    //    gotMessage(event.getAuthor(), event.getMessage(), null, event.getChannel());
    //}

    private void gotMessage(User author, Message msg, Guild guild, MessageChannel channel) {
        if(author.isBot()) {
            return;
        }
	List<Message.Attachment> atts = msg.getAttachments();
	for(Message.Attachment att : atts) {
		if(!att.isImage()) {
			continue;
		}
		if(!att.getFileName().toLowerCase().endsWith(".png")){
			return;
		}
		File f = new File("data_images", msg.getId() + "_" + att.getIdLong() + ".png");
		att.download(f);
		log.info(f.getName());
	}
    }

}
