/*

    Valora AI Moderator Bot
    Copyright (C) 2018  Discord Science and Technology Community

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package discordscience.valora;

import java.util.*;
import java.util.concurrent.*;
import net.cubekrowd.eventstorageapi.api.*;
import net.cubekrowd.eventstorageapi.common.storage.*;
import net.dv8tion.jda.core.*;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.utils.*;
import net.dv8tion.jda.core.requests.*;
import org.apache.logging.log4j.LogManager;
import discordscience.perspectiveapi.*;
import lombok.*;

@lombok.extern.log4j.Log4j2
public class ValoraBot {

    public static final String EMOTE_GOOD = "\u2705";
    public static final String EMOTE_WARNING = "\u26A0\uFE0F";
    public static final String EMOTE_BAD = "\u26D4";
    public static final String EMOTE_ROBOT = "\uD83D\uDEAB";
    public static final String EMOTE_TOXIC = "\u2623";

    public static void main(String[] args) {
        log.trace("TRACE");
        log.debug("DEBUG");
        log.error("ERROR");
        log.fatal("FATAL");
        log.info("INFO");
        log.warn("WARN");

        if(args.length == 0) {
            log.fatal("Missing Discord API key! Quitting...");
            System.exit(-1);
            return;
        } else if (args.length == 1) {
            new ValoraBot(args[0], null);
        } else {
            new ValoraBot(args[0], args[1]);
        }
    }

    @Getter
    private final ScheduledExecutorService scheduler;
    @Getter
    private JDA jda;
    @Getter
    private final PerspectiveAPI perspectiveAPI;

    public ValoraBot(String token, String key) {

        // Debug
        RestAction.setPassContext(true);
        RestAction.DEFAULT_FAILURE = Throwable::printStackTrace;

        // Load Scheduler
        log.info("===== Initialising bot =====");
        scheduler = Executors.newScheduledThreadPool(200);

        // Init ESAPI
        log.info("===== Opening ESAPI =====");
        EventStorageAPI.registerInit(new StorageMongoDB("mongodb", 27017, "valora", "", ""));
        EventStorageAPI.getStorage().open();

        // Init PerspecitveAPI
        log.info("===== Init PerspectiveAPI =====");
        if(key == null) {
            log.warn("Missing PerspectiveAPI key, skipping...");
            perspectiveAPI = null;
        } else {
            perspectiveAPI = new PerspectiveAPI(key, "discordscience");
        }

        log.info("===== Starting bot =====");
        // Connect to Discord
        try {
            JDABuilder jdab = new JDABuilder(AccountType.BOT)
            .setAudioEnabled(false)
            .setToken(token)
            .addEventListener(new PerspectiveListener(this))
            .addEventListener(new ImageListener(this));

            jda = jdab.buildBlocking();
            jda.getPresence().setStatus(OnlineStatus.ONLINE);
        } catch (Exception e) {
            log.fatal("Error while initializing bot", e);
            System.exit(-1);
        }

        // Setup shutdown hook
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                log.info("Running shutdown hook.");
                jda.shutdown();
                log.info("Shutdown hook done. Bye.");
                jda.shutdownNow();
                LogManager.shutdown();
            }
        });
    }

}
