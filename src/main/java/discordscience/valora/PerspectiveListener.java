/*

    Valora AI Moderator Bot
    Copyright (C) 2018  Discord Science and Technology Community

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package discordscience.valora;

import java.awt.Color;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.*;
import net.cubekrowd.eventstorageapi.api.*;
import net.dv8tion.jda.core.*;
import net.dv8tion.jda.core.hooks.*;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.*;
import net.dv8tion.jda.core.events.message.*;
import net.dv8tion.jda.core.events.message.guild.*;
import net.dv8tion.jda.core.events.message.guild.react.*;
import net.dv8tion.jda.core.events.message.priv.*;
import net.dv8tion.jda.core.events.message.priv.react.*;
import net.dv8tion.jda.core.events.guild.member.*;
import net.dv8tion.jda.core.events.guild.*;
import discordscience.perspectiveapi.*;
import lombok.*;

@lombok.extern.log4j.Log4j2
@RequiredArgsConstructor
public class PerspectiveListener extends ListenerAdapter {

    private final ValoraBot bot;

    @Override
    public void onReady(ReadyEvent event) {
        log.info("Connected to Discord.");
    }

    //public void onPrivateMessageReactionAdd(PrivateMessageReactionAddEvent event) {
    @Override
    public void onGuildMessageReactionAdd(GuildMessageReactionAddEvent event) {
        // Check if the reaction was made by this user
        if(event.getUser().getId().equals(bot.getJda().getSelfUser().getId())) {
            return;
        }
        event.getChannel().getMessageById(event.getMessageId()).queue(cardm -> {
            // Check if the message was made by this bot
            if(!cardm.getAuthor().getId().equalsIgnoreCase(bot.getJda().getSelfUser().getId())) {
                return;
            }
            // Reaction
            String reaction = event.getReactionEmote().getName();
            ReactAction ra = null;
            if(reaction.equals(ValoraBot.EMOTE_GOOD)) {
                ra = ReactAction.GOOD;
            } else if(reaction.equals(ValoraBot.EMOTE_BAD)) {
                ra = ReactAction.TOXIC;
            }
            if(ra != null) {
                reactMessage(cardm, ra, event.getUser());
            }
        });
    }

    private void reactMessage(@NonNull Message cardm, @NonNull ReactAction ra, @NonNull User reviewer) {
        if(cardm.getEmbeds().size() != 1) {
            return;
        }
        MessageEmbed card = cardm.getEmbeds().get(0);

        // Check card version
        String[] version = card.getFooter().getText().split(" ")[0].split(":");
        if(!version[0].equals("V") || !version[1].equals("3")) {
            log.debug("c ." + version[0] + "," + version[1]);
            return;
        }

        // Find original analyzed message
        Message message = null;
        Guild guild = bot.getJda().getGuildById(card.getFooter().getText().split(" ")[3].split(":")[1]);
        TextChannel channel = null;
        if(guild != null) {
            channel = guild.getTextChannelById(card.getFooter().getText().split(" ")[2].split(":")[1]);
            if(channel != null) {
                try {
                    message = channel.getMessageById(card.getFooter().getText().split(" ")[1].split(":")[1]).complete();
                } catch(net.dv8tion.jda.core.exceptions.ErrorResponseException e) {
                }
            }
        }

        String text = card.getFields().get(4).getValue();
        text = text.substring(7, text.length() - 3);

        if(ra == ReactAction.AUTO_WARN_DEL) {
            if(message != null) {
                message.delete().reason("Toxic message").queue();
                {
                    EmbedBuilder eb = new EmbedBuilder().setTitle(ValoraBot.EMOTE_WARNING + " Toxic Message Warning");
                    eb.setColor(Color.decode(ReactAction.AUTO_WARN_DEL.getColor()));
                    eb.addField("Warning:", "Your message has been deleted because it was detected to be severely toxic. Profanity, rude or inappropriate comments are not tolerated on this server.", false);
                    eb.addField("User:", card.getFields().get(0).getValue(), false);
                    channel.sendMessage(eb.build()).queue( message2 -> bot.getScheduler().schedule(() -> message2.delete().queue(), 15, TimeUnit.SECONDS));
                }
            }
            {
                EmbedBuilder eb = new EmbedBuilder().setTitle(ValoraBot.EMOTE_WARNING + " Toxic Message Warning");
                eb.setColor(Color.decode(ReactAction.AUTO_WARN_DEL.getColor()));
                eb.addField("Warning:", "Your message has been deleted because it was detected to be severely toxic. Profanity, rude or inappropriate comments are not tolerated on this server.", false);
                eb.addField("Message:", "```fix\n" + text.replaceAll("`", "` ") + "```", false);
                bot.getJda().getUserById(card.getFields().get(0).getValue().split("\\(")[1].split("\\)")[0]).openPrivateChannel().complete().sendMessage(eb.build()).queue();
            }
        }

        EmbedBuilder eb = new EmbedBuilder(card);
        eb.setTitle(ra.getEmoji() + " " + ra.getText()).setColor(Color.decode(ra.getColor()));
        eb.addField("Reviewer:", reviewer.getAsMention(), true);
        String[] ras = ra.name().split("_");
        eb.addField("Review:", guild == null ? (ras[0] + (ras.length > 1 ? "_" + ras[1] : "")) : (message == null ? ras[0] : ra.name()), true);
        eb.addField("Time:", System.currentTimeMillis() + "", true);
        if(bot.getPerspectiveAPI() != null) {
            bot.getPerspectiveAPI().suggestCommentScore(text, Map.of(Attribute.TOXICITY, ra.getSuggestedScore()));
        }
        cardm.editMessage(eb.build()).queue();
//            bot.getScheduler().schedule(() -> {try {
//                    cardm.getReactions().stream().filter(r -> (r.getReactionEmote().getName().equals(ValoraBot.EMOTE_GOOD) || r.getReactionEmote().getName().equals(ValoraBot.EMOTE_BAD))).forEach(r -> r.removeReaction().queue());
//                } catch(Exception e) {
//                    log.error("Error while removing reactions!", e);
//                }
//            }, 1, TimeUnit.SECONDS);

    }

    @Override
    public void onGuildMessageReceived(@NonNull GuildMessageReceivedEvent event) {
        gotMessage(event.getAuthor(), event.getMessage(), event.getGuild(), event.getChannel());
    }
    @Override
    public void onPrivateMessageReceived(@NonNull PrivateMessageReceivedEvent event) {
        gotMessage(event.getAuthor(), event.getMessage(), null, event.getChannel());
    }

    private void gotMessage(User author, Message msg, Guild guild, MessageChannel channel) {
        if(author.isBot()) {
            return;
        }
        String text = msg.getContentRaw();
        for(Message.MentionType mt : Message.MentionType.values()) {
            text = text.replaceAll(mt.getPattern().pattern(), "");
        }
        if(text.isEmpty()) {
            return;
        }
        text = text.substring(0, Math.min(1500, text.length()));

        // PAPI
        PerspectiveAPI.AttributeScoreList asl = bot.getPerspectiveAPI().analyzeComment(text, Attribute.TOXICITY, Attribute.SEVERE_TOXICITY, Attribute.THREAT, Attribute.SEXUALLY_EXPLICIT, Attribute.PROFANITY, Attribute.INCOHERENT, Attribute.OBSCENE, Attribute.LIKELY_TO_REJECT);

        // Store in db
        logAnalyzeResponse(msg, text, asl);

        // Debug log console
        for(Attribute a : asl.getSummaryScores().keySet()) {
            log.info(a.name() + " " + asl.getSummaryScore(a).getValue() + " [" + asl.getSpanScores(a).stream().map(ss -> "" + ss.getScore().getValue()).collect(Collectors.joining(",")) + "] " + msg.getId() + " " + text);
        }

	// do not moderate mods
        if(msg.getGuild() != null && msg.getMember().hasPermission(Permission.MESSAGE_MANAGE)) {
            return;
        }

        // send to audit channel
        double limit = text.toLowerCase().contains("stupid") ? 0.98d : 0.9d;
        boolean sum = asl.getSummaryScore(Attribute.TOXICITY).getValue() > limit ? true : false;
        boolean hardsum = asl.getSummaryScore(Attribute.TOXICITY).getValue() > 0.95d ? true : false;
        if(sum) {
            EmbedBuilder eb = new EmbedBuilder().setTitle(ValoraBot.EMOTE_WARNING + " Toxic Message Alert");
            eb.setTimestamp(msg.getCreationTime());
            eb.setColor(Color.decode("#FFC107"));
            eb.addField("Author:", msg.getAuthor().getName() + "#" + msg.getAuthor().getDiscriminator() + " (" + msg.getAuthor().getId() + ") " + msg.getAuthor().getAsMention(), false);
            eb.addField("Guild:", guild != null ? guild.getName() : "0", true);
            eb.addField("Channel:", channel.getName(), true);
            eb.addField("Toxicity:", String.format("%.2f", asl.getSummaryScore(Attribute.TOXICITY).getValue() * 100) + "%", true);

            eb.addField("Content:", "```fix\n" + text.replaceAll("`", "` ") + "```", false);
            eb.setFooter("V:3 MID:" + msg.getId() + " CID:" + msg.getChannel().getId() + " GID:" + (msg.getGuild() != null ? msg.getGuild().getId() : "0"), null);
            //bot.getJda().getUserById("92875770338213888").openPrivateChannel().complete().sendMessage(eb.build()).queue();
            bot.getJda().getGuildById("184140444677046274").getTextChannelById("460577792162004993").sendMessage(eb.build()).queue(m -> {
                if(hardsum) {
                    reactMessage(m, ReactAction.AUTO_WARN_DEL, bot.getJda().getSelfUser());
                }
                m.addReaction(ValoraBot.EMOTE_GOOD).complete();
                m.addReaction(ValoraBot.EMOTE_BAD).complete();
            });
        }
    }

    private void logAnalyzeResponse(Message message, String text, PerspectiveAPI.AttributeScoreList asl) {
        Map<String, String> data = new HashMap<>();
        data.put("message", message.getId());
        data.put("content", text);
        data.put("response", asl.toJSON().toString());
        data.put("author", message.getAuthor().getId());
        data.put("channel", message.getChannel().getId());
        data.put("guild", message.getGuild() != null ? message.getGuild().getId() : "-1");
        EventStorageAPI.getStorage().addEntry(new EventEntry("valora", "analyze", message.getCreationTime().toInstant().toEpochMilli(), data));
    }

}
