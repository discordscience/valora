package discordscience.valora;
import lombok.*;

@RequiredArgsConstructor
public enum ReactAction {
    GOOD(0.0d, ValoraBot.EMOTE_GOOD, "#4CAF50", "Message classified as OK"),
    TOXIC(1.0d, ValoraBot.EMOTE_BAD, "#F44336", "Message classified as TOXIC"),
    AUTO_WARN_DEL(0.95d, ValoraBot.EMOTE_ROBOT, "#9C27B0", "Message automatically classified as TOXIC");

    @Getter
    private final double suggestedScore;
    @Getter
    private final String emoji;
    @Getter
    private final String color;
    @Getter
    private final String text;
}

