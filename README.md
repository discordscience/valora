# Valora AI Moderator Bot

![Valora Logo](https://images.weserv.nl/?url=gitlab.com/discordscience/valora/raw/master/avatar.jpg&w=300)

## Introduction

Valora is an Esperanto name and means "valuable". The goal of this bot is to 
use AI-technology to classify and automatically detect toxic or otherwise 
unwanted comments on the server. **The bot is still under construction!**

![Unknown AI Review](https://images.weserv.nl/?url=gitlab.com/discordscience/valora/raw/master/review1.png&h=300)
![Bad AI Review](https://images.weserv.nl/?url=gitlab.com/discordscience/valora/raw/master/bad1.png&h=300)
