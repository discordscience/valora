#/bin/bash

# Stop any previous instance
docker stop valora

# Run as deamon with credentials
# $1 is discord-api-token
# $2 is perspecitve-api-key
docker run -d -it --rm --name valora -v "$PWD":/root/valora/ -w /root/valora/ openjdk:10-jre java --add-modules jdk.incubator.httpclient -jar target/valora.jar $1 $2

# Monitor logs
docker logs valora -f
