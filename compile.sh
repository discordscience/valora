#/bin/bash

# Compile in insolated conainter for replicability
docker run -it --rm -v "$PWD":/root/valora/ -v "$HOME"/.m2/:/root/.m2/ -w /root/valora/ maven:3-jdk-10-slim mvn -T10 package
